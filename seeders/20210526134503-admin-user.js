"use strict";
const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          uuid: "7c60299c-199d-4eb9-a36c-b233a3f9ae5a",
          email: "admin@rps.com",
          password: bcrypt.hashSync("PassworDRahasiA12!@", 12),
          role: "admin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", null, {});
  },
};
