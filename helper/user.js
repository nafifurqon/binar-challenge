const formatUserJwt = (user) => {
  const { uuid, email, role } = user;
  return { uuid, email, role, accessToken: user.generateToken() };
};

const getResult = (player1Hand, player2Hand) => {
  let result;
  player1Hand = player1Hand.toLowerCase();
  player2Hand = player2Hand.toLowerCase();

  if (player1Hand === player2Hand) {
    result = "draw";
  }

  if (player1Hand == "rock") {
    if (player2Hand == "paper") {
      result = "player 2 win";
    }

    if (player2Hand == "scissors") {
      result = "player 1 win";
    }
  }

  if (player1Hand == "paper") {
    if (player2Hand == "rock") {
      result = "player 1 win";
    }

    if (player2Hand == "scissors") {
      result = "player 2 win";
    }
  }

  if (player1Hand == "scissors") {
    if (player2Hand == "rock") {
      result = "player 2 win";
    }

    if (player2Hand == "paper") {
      result = "player 1 win";
    }
  }

  return result;
};

const getResultScore = (player1Score, player2Score) => {
  let result = "";

  if (player1Score === player2Score) {
    result = "draw";
  } else if (player1Score > player2Score) {
    result = "player 1 win";
  } else if (player2Score > player1Score) {
    result = "player 2 win";
  }

  return result;
};

const getRoomScore = (matches) => {
  let player1Score = 0;
  let player2Score = 0;

  matches.forEach((match) => {
    if (match.result.toLowerCase() === "player 1 win") {
      player1Score += 2;
      player2Score += 0;
    } else if (match.result.toLowerCase() === "player 2 win") {
      player1Score += 0;
      player2Score += 2;
    } else if (match.result.toLowerCase() === "draw") {
      player1Score += 1;
      player2Score += 1;
    }
  });

  const result = getResultScore(player1Score, player2Score);

  return { result, player1Score, player2Score };
};

module.exports = {
  formatUserJwt,
  getResult,
  getRoomScore,
};
