**1. Clone from online repository**
`git clone https://gitlab.com/nafifurqon/binar-challenge.git`

**2. Create config/config.json file base on config/config-example.json file**

**3. Install sequelize-cli**

```
npm install -g sequelize-cli
npm install --save sequelize
npm install --save-dev sequelize-cli
```

**4. Make sure there no database exists**
`sequelize db:drop`

**5. Create database using sequelize**
`sequelize db:create`

**6. Migrate to database**
`sequelize db:migrate`

**7. Create default admin**
`sequelize db:seed:all`

**8. Run app for development**
`npm run dev`

**Usage**

**Admin dashboard frontend**
[localhost:3000/admin](localhost:3000/admin)

**Games frontend**
[localhost:3000/](localhost:3000/)

**API (run on postman)**
[localhost:3000//api/v1](localhost:3000/api/v1)
