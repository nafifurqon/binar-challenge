"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.changeColumn(
        "matches",
        "player_1",
        {
          type: Sequelize.UUID,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "player_2",
        {
          type: Sequelize.UUID,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "player_1_hand",
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "player_2_hand",
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "result",
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
        { transaction }
      );
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.changeColumn(
        "matches",
        "player_1",
        {
          type: Sequelize.UUID,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "player_2",
        {
          type: Sequelize.UUID,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "player_1_hand",
        {
          type: Sequelize.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "player_2_hand",
        {
          type: Sequelize.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        "matches",
        "result",
        {
          type: Sequelize.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
