"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    queryInterface.addColumn("matches", "room_id", {
      type: Sequelize.UUID,
      allowNull: false,
      references: {
        model: "rooms",
        key: "uuid",
        onDelete: "cascade",
        onUpdate: "cascade",
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    queryInterface.removeColumn("matches", "room_id");
  },
};
