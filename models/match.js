"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Match extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const { User, Room } = models;
      this.belongsTo(User, { foreignKey: "player_1", as: "user_1" });
      this.belongsTo(User, { foreignKey: "player_2", as: "user_2" });
      this.belongsTo(Room, { foreignKey: "room_id", as: "room" });
    }
  }
  Match.init(
    {
      uuid: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      player_1: {
        type: DataTypes.UUID,
        allowNull: true,
        references: {
          model: "users",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      player_2: {
        type: DataTypes.UUID,
        allowNull: true,
        references: {
          model: "users",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      player_1_hand: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          notEmpty: {
            args: true,
            msg: "Hand cannot be empty",
          },
          isIn: {
            args: [["rock", "paper", "scissors"]],
            msg: "Hand must be one of rock, paper, or scissors",
          },
        },
      },
      player_2_hand: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          notEmpty: {
            args: true,
            msg: "Hand cannot be empty",
          },
          isIn: {
            args: [["rock", "paper", "scissors"]],
            msg: "Hand must be one of rock, paper, or scissors",
          },
        },
      },
      result: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      room_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "rooms",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
    },
    {
      sequelize,
      modelName: "Match",
      tableName: "matches",
    }
  );
  return Match;
};
