"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const { Match, User, Score } = models;
      this.hasMany(Match, { foreignKey: "room_id", as: "matches" });
      this.belongsTo(User, { foreignKey: "created_by", as: "user" });
      this.hasOne(Score, { foreignKey: "room_id", as: "score" });
    }
  }
  Room.init(
    {
      uuid: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      name: { type: DataTypes.STRING, allowNull: false },
      created_by: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "users",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
    },
    {
      sequelize,
      modelName: "Room",
      tableName: "rooms",
    }
  );
  return Room;
};
