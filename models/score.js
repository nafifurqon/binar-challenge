"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Score extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const { User, Room } = models;
      this.belongsTo(Room, { foreignKey: "room_id", as: "room" });
      this.belongsTo(User, { foreignKey: "player_1" });
      this.belongsTo(User, { foreignKey: "player_2" });
    }
  }
  Score.init(
    {
      uuid: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      player_1: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "users",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      player_2: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "users",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
      player_1_score: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      player_2_score: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      result: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      room_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "rooms",
          key: "uuid",
          onDelete: "cascade",
          onUpdate: "cascade",
        },
      },
    },
    {
      sequelize,
      modelName: "Score",
      tableName: "scores",
    }
  );
  return Score;
};
