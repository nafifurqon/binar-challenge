"use strict";
const { Model } = require("sequelize");

const bcrypt = require("../lib/bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ UserProfile, Match }) {
      this.hasOne(UserProfile, { foreignKey: "user_id", as: "user_profile" });
      this.hasMany(Match, { foreignKey: "player_1", as: "player_1_match" });
      this.hasMany(Match, { foreignKey: "player_2", as: "player_2_match" });
    }

    static #encrypt = (password) => bcrypt.encrypt(password, 12);

    checkPassword = (password) => bcrypt.compare(password, this.password);

    static register = async ({ email, password, role }) => {
      const encryptedPassword = this.#encrypt(password);

      return this.create({ email, password: encryptedPassword, role });
    };

    static authenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({ where: { email } });
        if (!user) return Promise.reject("User not registered");

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong password");

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };

    generateToken = () => {
      const payload = {
        uuid: this.uuid,
        email: this.email,
        role: this.role,
      };

      const secretKey =
        "21ad0e385453a5ccf6acd175c4189672735752822b513c8b82e19b4744fb0830d0c3f3d34e142dbbbca85b53dfb54c599a60bebb39eae8487bb66e1e6ac77104";

      const token = jwt.sign(payload, secretKey);
      return token;
    };
  }
  User.init(
    {
      uuid: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: {
          args: true,
          msg: "Email already registered",
        },
        validate: {
          isEmail: {
            args: true,
            msg: "Please input a valid email.",
          },
          notEmpty: {
            args: true,
            msg: "Email cannot be empty.",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            args: true,
            msg: "Password cannot be empty.",
          },
        },
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            args: true,
            msg: "Role cannot be empty.",
          },
          isIn: {
            args: [["admin", "player"]],
            msg: "Role must be one of admin and player",
          },
        },
      },
    },
    {
      sequelize,
      modelName: "User",
      tableName: "users",
    }
  );
  return User;
};
