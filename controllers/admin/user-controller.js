const { User } = require("../../models");
const bcrypt = require("../../lib/bcrypt");

const roles = ["admin", "player"];

const showAllUsers = async (req, res) => {
  try {
    const users = await User.findAll();

    const alertMessage = req.flash("alertMessage");
    const alertStatus = req.flash("alertStatus");
    const alert = { message: alertMessage, status: alertStatus };

    res.status(200).render("admin/user/view-users", {
      users,
      roles,
      userLogin: req.user.dataValues,
      title: "RPS Admin | User",
      alert,
    });
  } catch (error) {
    console.log(error);
    req.flash("alertMessage", `${error.message}`);
    req.flash("alertStatus", "danger");
    res.redirect("/admin/users");
  }
};

const createUser = async (req, res) => {
  try {
    const { email, password, role } = req.body;

    const encryptedPassword = bcrypt.encrypt(password, 12);

    await User.create({
      email,
      password: encryptedPassword,
      role,
    });

    req.flash("alertMessage", "Successfully add user.");
    req.flash("alertStatus", "success");
    res.redirect("/admin/users");
  } catch (error) {
    console.log(error);
    req.flash("alertMessage", `${error.message}.`);
    req.flash("alertStatus", "danger");
    res.redirect("/admin/users");
  }
};

const updateUser = async (req, res) => {
  try {
    const { email, password, role, uuid } = req.body;

    const encryptedPassword = bcrypt.encrypt(password, 12);

    await User.update(
      { email, password: encryptedPassword, role },
      { where: { uuid } }
    );

    req.flash("alertMessage", "Successfully update user.");
    req.flash("alertStatus", "success");
    res.redirect("/admin/users");
  } catch (error) {
    console.log(error);
    req.flash("alertMessage", `${error.message}.`);
    req.flash("alertStatus", "danger");
    res.redirect("/admin/users");
  }
};

const deleteUser = async (req, res) => {
  try {
    console.log(req.params.uuid);
    const uuid = req.params.uuid;
    await User.destroy({
      where: { uuid },
    });

    req.flash("alertMessage", "Successfully delete user.");
    req.flash("alertStatus", "success");
    res.redirect("/admin/users");
  } catch (error) {
    console.log(error);
    req.flash("alertMessage", `${error.message}.`);
    req.flash("alertStatus", "danger");
    res.redirect("/admin/users");
  }
};

module.exports = {
  showAllUsers,
  createUser,
  updateUser,
  deleteUser,
};
