const { Room, User } = require("../../models");
const alert = require("../../responses/alert");

const showAllRooms = async (req, res) => {
  try {
    const rooms = await Room.findAll({ include: "user" });

    const users = await User.findAll({
      attributes: ["uuid", "email"],
      where: { role: "player" },
    });

    const alertMessage = req.flash("alertMessage");
    const alertStatus = req.flash("alertStatus");
    const alert = { message: alertMessage, status: alertStatus };

    res.status(200).render("admin/room/view-room", {
      users,
      rooms,
      userLogin: req.user.dataValues,
      title: "RPS Admin | Room",
      alert,
    });
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/rooms");
  }
};

const createRoom = async (req, res) => {
  try {
    const { name, created_by } = req.body;

    await Room.create({ name, created_by });

    alert.alertSuccess(req, res, "Successfully add room.", "/admin/rooms");
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/rooms");
  }
};

const updateRoom = async (req, res) => {
  try {
    const { uuid, name, created_by } = req.body;

    await Room.update({ name, created_by }, { where: { uuid } });

    alert.alertSuccess(req, res, "Successfully update room.", "/admin/rooms");
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/rooms");
  }
};

const deleteRoom = async (req, res) => {
  try {
    const { uuid } = req.params;

    await Room.destroy({ where: { uuid } });

    alert.alertSuccess(req, res, "Successfully delete room.", "/admin/rooms");
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/rooms");
  }
};

module.exports = { showAllRooms, createRoom, updateRoom, deleteRoom };
