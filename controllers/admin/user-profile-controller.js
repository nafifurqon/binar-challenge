const { QueryTypes } = require("sequelize");
const { UserProfile, sequelize } = require("../../models");
const alert = require("../../responses/alert");

const showAllUserProfiles = async (req, res) => {
  try {
    const userProfiles = await UserProfile.findAll({
      include: "user",
    });

    const users = await sequelize.query(
      "select uuid, email from users where role = 'player' and uuid not in ( select user_id from user_profiles )",
      { type: QueryTypes.SELECT }
    );

    const alertMessage = req.flash("alertMessage");
    const alertStatus = req.flash("alertStatus");
    const alert = { message: alertMessage, status: alertStatus };

    res.status(200).render("admin/user-profile/view-user-profiles", {
      userProfiles,
      users,
      userLogin: req.user.dataValues,
      title: "RPS Admin | User Profile",
      alert,
    });
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/user-profiles");
  }
};

const createUserProfile = async (req, res) => {
  try {
    const { fullName, job, bio, userId } = req.body;

    await UserProfile.create({
      full_name: fullName,
      job,
      bio,
      user_id: userId,
    });

    alert.alertSuccess(
      req,
      res,
      "Successfully add user profile.",
      "/admin/user-profiles"
    );
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/user-profiles");
  }
};

const updateUserProfile = async (req, res) => {
  try {
    const { uuid, fullName, job, bio, userId } = req.body;

    await UserProfile.update(
      {
        full_name: fullName,
        job,
        bio,
        user_id: userId,
      },
      {
        where: { uuid },
      }
    );

    alert.alertSuccess(
      req,
      res,
      "Successfully update user profile.",
      "/admin/user-profiles"
    );
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/user-profiles");
  }
};

const deleteUserProfile = async (req, res) => {
  try {
    const uuid = req.params.uuid;

    await UserProfile.destroy({
      where: { uuid },
    });

    alert.alertSuccess(
      req,
      res,
      "Successfully delete user profile.",
      "/admin/user-profiles"
    );
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/user-profiles");
  }
};

module.exports = {
  showAllUserProfiles,
  createUserProfile,
  updateUserProfile,
  deleteUserProfile,
};
