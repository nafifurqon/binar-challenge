const { User, Match, Room } = require("../../models");
const defaultAdmin = require("./default-admin.json");
const passport = require("passport");

const showDashboardHome = async (req, res) => {
  try {
    const countPlayers = await User.count({
      col: "uuid",
      where: { role: "player" },
    });

    const countMatches = await Match.count({ col: "uuid" });

    const countRooms = await Room.count({ col: "uuid" });

    res.status(200).render("admin/dashboard/dashboard", {
      countPlayers,
      countMatches,
      countRooms,
      userLogin: req.user.dataValues,
      title: "RPS Admin | Dashboard",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
};

const showLoginPage = async (req, res) => {
  try {
    await res.status(200).render("admin/auth/login", {
      title: "RPS Admin | Login",
      defaultAdmin,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
};

const actionLogin = passport.authenticate("local", {
  successRedirect: "/admin",
  failureRedirect: "/admin/auth/login",
  failureFlash: true,
});

const actionLogout = async (req, res) => {
  try {
    req.logout();
    res.redirect("/admin/auth/login");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  showDashboardHome,
  showLoginPage,
  actionLogin,
  actionLogout,
};
