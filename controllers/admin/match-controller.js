const { Match, User, Room, sequelize } = require("../../models");
const alert = require("../../responses/alert");

const hands = ["rock", "paper", "scissors"];
const results = ["Player 1 Win", "Player 2 Win", "Draw"];

const showAllMatches = async (req, res) => {
  try {
    const matches = await Match.findAll({
      include: ["user_1", "user_2", "room"],
    });

    const users = await User.findAll({
      attributes: ["uuid", "email"],
      where: { role: "player" },
    });

    const rooms = await Room.findAll({
      attributes: ["uuid", "name"],
    });

    const alertMessage = req.flash("alertMessage");
    const alertStatus = req.flash("alertStatus");
    const alert = { message: alertMessage, status: alertStatus };

    res.status(200).render("admin/match/view-match", {
      matches,
      users,
      hands,
      results,
      rooms,
      userLogin: req.user.dataValues,
      title: "RPS Admin | Match",
      alert,
    });
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/matches");
  }
};

const createMatch = async (req, res) => {
  try {
    const { player1, player2, player1Hand, player2Hand, result, room } =
      req.body;

    await Match.create({
      player_1: player1,
      player_2: player2,
      player_1_hand: player1Hand,
      player_2_hand: player2Hand,
      result,
      room_id: room,
    });

    alert.alertSuccess(req, res, "Successfully add matches.", "/admin/matches");
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/matches");
  }
};

const updateMatch = async (req, res) => {
  try {
    const { uuid, player1, player2, player1Hand, player2Hand, result } =
      req.body;

    await Match.update(
      {
        player_1: player1,
        player_2: player2,
        player_1_hand: player1Hand,
        player_2_hand: player2Hand,
        result,
      },
      {
        where: { uuid },
      }
    );

    alert.alertSuccess(
      req,
      res,
      "Successfully update matches.",
      "/admin/matches"
    );
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/matches");
  }
};

const deleteMatch = async (req, res) => {
  try {
    const uuid = req.params.uuid;

    await Match.destroy({
      where: { uuid },
    });

    alert.alertSuccess(
      req,
      res,
      "Successfully delete matches.",
      "/admin/matches"
    );
  } catch (error) {
    console.log(error);
    alert.alertDanger(req, res, `${error.message}`, "/admin/matches");
  }
};

module.exports = {
  showAllMatches,
  createMatch,
  updateMatch,
  deleteMatch,
};
