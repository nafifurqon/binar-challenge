const { QueryTypes } = require("sequelize");
const { User, Room, Match, Score } = require("../../models");
const userHelper = require("../../helper/user");
const apiResponse = require("../../responses/api");
const bcrypt = require("../../lib/bcrypt");

const test = async (req, res) => {
  const currentUser = userHelper.formatUserJwt(req.user);
  res.json(currentUser);
};

const getAllUsers = async (req, res) => {
  try {
    const users = await User.findAll({
      include: ["user_profile", "player_1_match", "player_2_match"],
    });

    apiResponse.success(res, 200, "success get data users", users);
  } catch (error) {
    console.log(error);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const createUser = async (req, res) => {
  try {
    const { email, password, role } = req.body;

    const user = await User.register({ email, password, role });

    apiResponse.success(res, 201, "success create user", user);
  } catch (error) {
    console.log(error.message);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const getUserById = async (req, res) => {
  try {
    const uuid = req.params.uuid;

    const user = await User.findOne({
      include: ["user_profile", "player_1_match", "player_2_match"],
      where: { uuid },
    });

    apiResponse.success(res, 200, "success get data user", user);
  } catch (error) {
    console.log(error);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.authenticate({ email, password });

    if (user && user.role !== "player")
      return apiResponse.failed(
        res,
        403,
        "Access denied. Only for player.",
        null
      );

    apiResponse.success(
      res,
      200,
      "success login user",
      userHelper.formatUserJwt(user)
    );
  } catch (error) {
    console.log(error);
    apiResponse.failed(res, 500, error || error.message, null);
  }
};

const updateUser = async (req, res) => {
  try {
    const { email, password, role } = req.body;
    const uuid = req.params.uuid;
    const userLogin = req.user.dataValues;

    if (uuid !== userLogin.uuid) {
      return apiResponse.failed(
        res,
        403,
        "Access denied. Can update your data only.",
        null
      );
    }

    let user = await User.findOne({
      where: { uuid },
    });

    if (!user) {
      return apiResponse.failed(res, 404, "User not found", null);
    }

    const encryptedPassword = bcrypt.encrypt(password, 12);
    user = await User.update(
      { email, password: encryptedPassword, role },
      { where: { uuid } }
    );
    user = await User.findOne({ where: { uuid } });
    console.log(user);

    apiResponse.success(res, 200, "Success update user data.", user);
  } catch (error) {
    console.log(error);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const deleteUser = async (req, res) => {
  try {
    const uuid = req.params.uuid;
    const userLogin = req.user.dataValues;

    if (uuid !== userLogin.uuid) {
      return apiResponse.failed(
        res,
        403,
        "Access denied. Can delete your data only.",
        null
      );
    }

    let user = await User.findOne({
      where: { uuid },
    });

    if (!user) {
      return apiResponse.failed(res, 404, "User not found", null);
    }

    user = await User.destroy({
      where: { uuid },
    });

    apiResponse.success(res, 200, `Success delete data user with ${uuid}`, {});
  } catch (error) {
    console.log(error);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const createRoom = async (req, res) => {
  try {
    const { name } = req.body;
    const { uuid } = req.user.dataValues;

    const room = await Room.create({ name, created_by: uuid });

    return apiResponse.success(res, 201, "success create room", room);
  } catch (error) {
    console.log(error.message);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const roomMatchAction = async (req, res) => {
  try {
    const { room_id } = req.params;
    const { hand } = req.body;
    const { uuid } = req.user.dataValues;

    let room = await Room.findByPk(room_id, {
      include: [
        { model: Match, as: "matches", include: ["user_1", "user_2"] },
        "score",
      ],
    });
    if (!room) return apiResponse.failed(res, 404, "Room not found", null);

    let matches = await Match.findAll({ where: { room_id } });
    let matchesLength = matches.length;
    let match;
    let score;

    const maxMatch = 3;

    if (matchesLength === maxMatch && matches[maxMatch - 1].result) {
      return apiResponse.success(
        res,
        200,
        "This room has already play 3 matches",
        room
      );
    }

    if (matchesLength === 0) {
      match = await Match.create({ room_id });
    } else if (!matches[matchesLength - 1].result) {
      match = await Match.findByPk(matches[matchesLength - 1].uuid);
    } else if (matches[matchesLength - 1].result) {
      console.log(`matchesLength: ${matchesLength}`);
      match = await Match.create({ room_id });
    }

    let player1;
    let player1Hand;
    let player2;
    let player2Hand;

    if (uuid === room.created_by) {
      player1 = uuid;
      player1Hand = hand;

      if (match) {
        const updatePlayer1 = await Match.update(
          {
            player_1: player1,
            player_1_hand: player1Hand,
          },
          { where: { uuid: match.uuid } }
        );
      }
    }

    if (uuid !== room.created_by) {
      player2 = uuid;
      player2Hand = hand;

      if (match) {
        const updatePlayer2 = await Match.update(
          {
            player_2: player2,
            player_2_hand: player2Hand,
          },
          { where: { uuid: match.uuid } }
        );
      }
    }

    match = await Match.findByPk(match.uuid);
    if (match) {
      room = await Room.findByPk(room_id, {
        include: [
          { model: Match, as: "matches", include: ["user_1", "user_2"] },
          "score",
        ],
      });

      if (!match.player_1 || !match.player_2) {
        return apiResponse.success(
          res,
          200,
          "Waiting for other player turn.",
          room
        );
      }

      const result = userHelper.getResult(
        match.player_1_hand,
        match.player_2_hand
      );

      const matchResult = await Match.update(
        { result },
        { where: { uuid: match.uuid } }
      );

      matches = await Match.findAll({ where: { room_id } });
      matchesLength = matches.length;
      const roomScore = userHelper.getRoomScore(matches);

      if (matchesLength === 1) {
        score = await Score.create({
          player_1: matches[matchesLength - 1].player_1,
          player_2: matches[matchesLength - 1].player_2,
          player_1_score: roomScore.player1Score,
          player_2_score: roomScore.player2Score,
          result: roomScore.result,
          room_id,
        });
      } else {
        score = await Score.update(
          {
            player_1: matches[matchesLength - 1].player_1,
            player_2: matches[matchesLength - 1].player_2,
            player_1_score: roomScore.player1Score,
            player_2_score: roomScore.player2Score,
            result: roomScore.result,
            room_id,
          },
          { where: { room_id } }
        );
      }
      room = await Room.findByPk(room_id, {
        include: [
          { model: Match, as: "matches", include: ["user_1", "user_2"] },
          "score",
        ],
      });
    }

    apiResponse.success(res, 200, "Success play match", room);
  } catch (error) {
    console.log(error);
    apiResponse.failed(res, 500, error.message, null);
  }
};

const getRoomById = async (req, res) => {
  try {
    const roomId = req.params.room_id;
    const userId = req.user.uuid;

    const room = await Room.findByPk(roomId, {
      include: [
        { model: Match, as: "matches", include: ["user_1", "user_2"] },
        "score",
      ],
    });

    apiResponse.success(res, 201, "success get room by id", room);
  } catch (error) {
    console.log(error.message);
    apiResponse.failed(res, 500, error.message, null);
  }
};

module.exports = {
  getAllUsers,
  createUser,
  loginUser,
  updateUser,
  deleteUser,
  getUserById,
  test,
  createRoom,
  roomMatchAction,
  getRoomById,
};
