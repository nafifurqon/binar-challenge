const { User } = require("../../models");
const userHelper = require("../../helper/user");
const passport = require("passport");

const getHomePage = async (req, res) => {
  try {
    const user = req.user;

    res.render("game/home", {
      user,
      title: "RPS Game | Home",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: error.message,
      data: null,
    });
  }
};

const getGamesPage = async (req, res) => {
  try {
    res.render("game/games", {
      title: "RPS Game | Games",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: error.message,
      data: null,
    });
  }
};

const getRegisterPage = async (req, res) => {
  try {
    res.render("users/register", {
      errorMessage: "",
      title: "RPS Game | Register",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: error.message,
      data: null,
    });
  }
};

const getLoginPage = async (req, res) => {
  try {
    res.render("users/login", {
      errorMessage: "",
      title: "RPS Game | Login",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: error.message,
      data: null,
    });
  }
};

const createUserGames = async (req, res) => {
  try {
    const { email, password } = req.body;

    let user = await User.findOne({
      where: { email },
    });

    if (user) {
      res.status(409).render("users/register", {
        errorMessage: "User is already registered. Please login.",
        title: "RPS Game | Register",
      });
      return;
    }

    await User.register({ email, password, role: "player" });

    await res.status(201).redirect("/login");
    return;
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: error.message,
      data: null,
    });
  }
};

const loginUserGames = passport.authenticate("local", {
  successRedirect: "/",
  failureRedirect: "/login",
  failureFlash: true,
});

const logoutUserGames = (req, res) => {
  try {
    req.logout();
    res.redirect("/");
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: error.message,
      data: null,
    });
  }
};

module.exports = {
  getHomePage,
  getGamesPage,
  getRegisterPage,
  getLoginPage,
  createUserGames,
  loginUserGames,
  logoutUserGames,
};
