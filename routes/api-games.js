const express = require("express");
const router = express.Router();
const userAPI = require("../controllers/api/user-controller");
const restrict = require("../middlewares/restrict");
const validate = require("../middlewares/validator");

// User
router.get("/users", userAPI.getAllUsers);
router.get("/users/:uuid", userAPI.getUserById);
router.post("/users/register", validate.validateUser, userAPI.createUser);
router.post("/users/login", validate.validateLogin, userAPI.loginUser);
router.put(
  "/users/:uuid",
  restrict.restrictUser,
  validate.validateUser,
  userAPI.updateUser
);
router.delete("/users/:uuid", restrict.restrictUser, userAPI.deleteUser);
router.get("/test", restrict.restrictUser, userAPI.test);

// Room Match
router.post("/room", restrict.restrictUser, userAPI.createRoom);
router.post(
  "/room/fight/:room_id",
  restrict.restrictUser,
  validate.validateRoomFight,
  userAPI.roomMatchAction
);
router.get("/room/:room_id", restrict.restrictUser, userAPI.getRoomById);

module.exports = router;
