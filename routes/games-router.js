const express = require("express");
const router = express.Router();
const gamesController = require("../controllers/frontend/games-controller");
const restrict = require("../middlewares/restrict");

router.get("/", gamesController.getHomePage);
router.get("/games", restrict.restrictGame, gamesController.getGamesPage);
router.get("/register", gamesController.getRegisterPage);
router.get("/login", gamesController.getLoginPage);
router.post("/register", gamesController.createUserGames);
router.post("/login", gamesController.loginUserGames);
router.get("/logout", gamesController.logoutUserGames);

module.exports = router;
