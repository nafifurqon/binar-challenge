const adminRouter = require("./admin-router");
const gamesRouter = require("./games-router");
const apiGamesRouter = require("./api-games");

module.exports = {
  adminRouter,
  gamesRouter,
  apiGamesRouter,
};
