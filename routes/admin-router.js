const express = require("express");
const router = express.Router();
const userController = require("../controllers/admin/user-controller");
const userProfile = require("../controllers/admin/user-profile-controller");
const matchController = require("../controllers/admin/match-controller");
const authController = require("../controllers/admin/admin-auth-controller");
const roomController = require("../controllers/admin/room-controller");
const restrict = require("../middlewares/restrict");
router.use(express.json());

// Dashboard Home
router.get("/", restrict.restrictAdmin, authController.showDashboardHome);
router.get("/auth/login", authController.showLoginPage);
router.post("/auth/login", authController.actionLogin);
router.get("/auth/logout", authController.actionLogout);

// User
router.get("/users", restrict.restrictAdmin, userController.showAllUsers);
router.post("/users", userController.createUser);
router.put("/users", userController.updateUser);
router.delete("/users/:uuid", userController.deleteUser);

//User Profile
router.get(
  "/user-profiles",
  restrict.restrictAdmin,
  userProfile.showAllUserProfiles
);
router.post("/user-profiles", userProfile.createUserProfile);
router.put("/user-profiles", userProfile.updateUserProfile);
router.delete("/user-profiles/:uuid", userProfile.deleteUserProfile);

//Match
router.get("/matches", restrict.restrictAdmin, matchController.showAllMatches);
router.post("/matches", matchController.createMatch);
router.put("/matches", matchController.updateMatch);
router.delete("/matches/:uuid", matchController.deleteMatch);

//Room
router.get("/rooms", restrict.restrictAdmin, roomController.showAllRooms);
router.post("/rooms", roomController.createRoom);
router.put("/rooms", roomController.updateRoom);
router.delete("/rooms/:uuid", roomController.deleteRoom);

module.exports = router;
