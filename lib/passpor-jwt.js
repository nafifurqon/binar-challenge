const passportJwt = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { User } = require("../models");

// create secret using console.log(require("crypto").randomBytes(64).toString("hex"));
const options = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey:
    "21ad0e385453a5ccf6acd175c4189672735752822b513c8b82e19b4744fb0830d0c3f3d34e142dbbbca85b53dfb54c599a60bebb39eae8487bb66e1e6ac77104",
};

passportJwt.use(
  new JwtStrategy(options, async (payload, done) => {
    try {
      let user = await User.findByPk(payload.uuid);

      if (user && user.role === "player") done(null, user);
      else done(null, false);
    } catch (error) {
      done(error, false);
    }
  })
);

module.exports = passportJwt;
