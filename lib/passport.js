const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const { User } = require("../models");

const authenticate = async (email, password, done) => {
  try {
    const user = await User.authenticate({ email, password });

    return done(null, user);
  } catch (error) {
    console.log(error.message);
    return done(null, false, { message: error.message });
  }
};

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    authenticate
  )
);

passport.serializeUser((user, done) => done(null, user.uuid));

passport.deserializeUser(async (uuid, done) =>
  done(null, await User.findByPk(uuid))
);

module.exports = passport;
