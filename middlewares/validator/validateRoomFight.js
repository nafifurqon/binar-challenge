const { check, validationResult } = require("express-validator");
const apiResponse = require("../../responses/api");

const rules = [
  check("hand")
    .notEmpty()
    .withMessage("Hand is required")
    .isIn(["rock", "paper", "scissors"])
    .withMessage("Hand must be one of rock, paper, or scissors")
    .trim()
    .escape(),
];

const validateRoomFight = [
  // Rules
  rules,
  // Response
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return apiResponse.failed(res, 422, "validate request", {
        errors: errors.array(),
      });
    }
    next();
  },
];

module.exports = validateRoomFight;
