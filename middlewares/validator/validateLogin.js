const { check, validationResult } = require("express-validator");
const apiResponse = require("../../responses/api");

const rules = [
  check("email")
    .notEmpty()
    .withMessage("Email is required!")
    .isEmail()
    .withMessage("Email must be valid!")
    .normalizeEmail()
    .trim()
    .escape(),

  check("password")
    .notEmpty()
    .withMessage("Password is required!")
    .isLength({ min: 10 })
    .withMessage("Password at least 10 characters!")
    .isString()
    .withMessage("Password must contain letter!")
    .trim()
    .escape(),
];

const validateLogin = [
  // Rules
  rules,
  // Response
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return apiResponse.failed(res, 422, "validate request", {
        errors: errors.array(),
      });
    }
    next();
  },
];

module.exports = validateLogin;
