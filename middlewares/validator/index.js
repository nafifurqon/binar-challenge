const validateLogin = require("./validateLogin");
const validateUser = require("./validateUser");
const validateRoomFight = require("./validateRoomFight");

module.exports = { validateLogin, validateUser, validateRoomFight };
