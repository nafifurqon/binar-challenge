const passportJwt = require("../lib/passpor-jwt");

const restrictAdmin = (req, res, next) => {
  if (req.isAuthenticated() && req.user.role !== "admin")
    return res.redirect("/admin/auth/login");

  if (req.isAuthenticated()) return next();
  res.redirect("/admin/auth/login");
};

const restrictGame = (req, res, next) => {
  const user = req.user.dataValues;
  if (user.role !== "player") return res.redirect("/login");

  if (req.isAuthenticated()) return next();
  res.redirect("/");
};

const restrictUser = passportJwt.authenticate("jwt", {
  session: false,
});

module.exports = { restrictAdmin, restrictGame, restrictUser };
